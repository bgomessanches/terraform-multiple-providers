
provider "aws" {
  region  = "sa-east-1"
  alias   = "aws_prod"
  profile = "prodprofile"
}

provider "aws" {
  region  = "sa-east-2"
  alias   = "aws_hom"
  profile = "homprofile"
}

provider "aws" {
  region  = "us-east-1"
  alias   = "aws_dev"
  profile = "devprofile"
}