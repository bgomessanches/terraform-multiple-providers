module "ec2-dev" {
  source      = "./modules/ec2"
  environment = "dev"

  providers = {
    aws = aws.dev
  }
}

module "ec2-hom" {
  source      = "./modules/ec2"
  environment = "hom"

  providers = {
    aws = aws.hom
  }
}

module "ec2-prod" {
  source      = "./modules/ec2"
  environment = "prod"

  providers = {
    aws = aws.prod
  }
}

module "vm" {
  source  = "terraform-google-modules/vm/google"
  version = "6.4.0"
}

module "vm" {
  source  = "terraform-google-modules/vm/google"
  version = "6.4.0"
}

module "vm" {
  source  = "terraform-google-modules/vm/google"
  version = "6.4.0"
}
